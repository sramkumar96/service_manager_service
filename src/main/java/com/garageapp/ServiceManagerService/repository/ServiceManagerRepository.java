package com.garageapp.ServiceManagerService.repository;

import com.garageapp.ServiceManagerService.models.Service_Manager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ServiceManagerRepository extends JpaRepository< Service_Manager, Long> {
    Optional<Service_Manager> findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);

    Service_Manager findByEmail(String email);
}
