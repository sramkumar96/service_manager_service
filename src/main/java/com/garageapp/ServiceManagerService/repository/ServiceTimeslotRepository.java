package com.garageapp.ServiceManagerService.repository;

import com.garageapp.ServiceManagerService.models.Service_Manager;
import com.garageapp.ServiceManagerService.models.Service_Timeslot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ServiceTimeslotRepository extends JpaRepository<Service_Timeslot,Long> {
    Optional<Service_Timeslot> findById(Long timeslot_id);
}