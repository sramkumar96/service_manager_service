package com.garageapp.ServiceManagerService.repository;

import com.garageapp.ServiceManagerService.models.Booking;
import com.garageapp.ServiceManagerService.models.Service_Timeslot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {
}
