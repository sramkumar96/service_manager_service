package com.garageapp.ServiceManagerService.models;

import javax.persistence.*;

@Entity
@Table(name = "booking")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long booking_id;
    private Long timeslot_id;
    private Long customer_id;
    private Long vehicle_id;

    public Booking() {
    }

    public Booking(Long timeslot_id, Long customer_id, Long vehicle_id) {
        this.timeslot_id = timeslot_id;
        this.customer_id = customer_id;
        this.vehicle_id = vehicle_id;
    }



    public Long getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(Long booking_id) {
        this.booking_id = booking_id;
    }

    public Long getTimeslot_id() {
        return timeslot_id;
    }

    public void setTimeslot_id(Long timeslot_id) {
        this.timeslot_id = timeslot_id;
    }

    public Long getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Long customer_id) {
        this.customer_id = customer_id;
    }

    public Long getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(Long vehicle_id) {
        this.vehicle_id = vehicle_id;
    }


}
