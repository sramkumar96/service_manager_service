package com.garageapp.ServiceManagerService.controllers;

import com.garageapp.ServiceManagerService.models.Booking;
import com.garageapp.ServiceManagerService.models.Service_Timeslot;
import com.garageapp.ServiceManagerService.payload.request.BookingRequest;
import com.garageapp.ServiceManagerService.payload.request.CreateTimeslotRequest;
import com.garageapp.ServiceManagerService.payload.response.MessageResponse;
import com.garageapp.ServiceManagerService.repository.BookingRepository;
import com.garageapp.ServiceManagerService.repository.ServiceTimeslotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/booking")
public class BookingController {

    @Autowired
    BookingRepository bookingrepository;

    @Autowired
    ServiceTimeslotRepository servicetimeslotrepository;

    @PostMapping("/createbooking")
    public ResponseEntity<?> createBooking(@RequestBody BookingRequest bookingrequest) {

        Booking booking = new Booking(
                bookingrequest.getTimeslot_id(),
                bookingrequest.getCustomer_id(),
                bookingrequest.getVehicle_id());
        bookingrepository.save(booking);

        Optional<Service_Timeslot> existingServiceTimeslot =  servicetimeslotrepository.findById( bookingrequest.getTimeslot_id());
        existingServiceTimeslot.ifPresent((Service_Timeslot timeslot) -> {
            timeslot.setNum_of_booking(timeslot.getNum_of_booking() + 1);
            servicetimeslotrepository.save(timeslot);
        });
        return ResponseEntity.ok(new MessageResponse("Booked Successfully!"));
    }

    @GetMapping("/getbookinglist/{customerId}")
    public List<Booking> getBookedTimeslots(@PathVariable Long customerId) {
        List<Booking> allBookedTimeslots = bookingrepository.findAll();
        List<Booking> filteredBookingList = allBookedTimeslots.stream()
                .filter(timeslot -> timeslot.getCustomer_id().longValue() == customerId.longValue())
                .collect(Collectors.toList());
        return filteredBookingList;
    }
}

//    @GetMapping("/laptops/name")
//    public ResponseEntity<List<Laptop>> getLaptopsByName(@RequestParam String name) {
//        return new ResponseEntity<List<Laptop>>(lRepo.findByName(name), HttpStatus.OK);
//    }