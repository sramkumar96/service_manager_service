package com.garageapp.ServiceManagerService.controllers;

import com.garageapp.ServiceManagerService.models.Service_Timeslot;
import com.garageapp.ServiceManagerService.payload.request.CreateTimeslotRequest;
import com.garageapp.ServiceManagerService.payload.response.MessageResponse;
import com.garageapp.ServiceManagerService.repository.ServiceTimeslotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/timeslot")
public class TimeSlotController {

    @Autowired
    ServiceTimeslotRepository servicetimeslotrepository;

    @PostMapping("/createtimeslot")
    public ResponseEntity<?> createTimeSlot(@RequestBody CreateTimeslotRequest createtimeslotrequest) {

        Service_Timeslot service_timeslot = new Service_Timeslot(
                createtimeslotrequest.getManager_Id(),
                createtimeslotrequest.getManager_Username(),
                createtimeslotrequest.getDate(),
                createtimeslotrequest.getStart_time(),
                createtimeslotrequest.getEnd_time(),
                createtimeslotrequest.getBooking_limit(),
                createtimeslotrequest.getNum_of_booking());

        servicetimeslotrepository.save(service_timeslot);
        return ResponseEntity.ok(new MessageResponse("Service Time Slot Created Successfully!"));
    }

    @GetMapping("/alltimeslots")
    public List<Service_Timeslot> getAllTimeslots() {
        return servicetimeslotrepository.findAll();
    }
}
