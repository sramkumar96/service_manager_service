package com.garageapp.ServiceManagerService.controllers;

import com.garageapp.ServiceManagerService.models.Service_Manager;
import com.garageapp.ServiceManagerService.payload.request.LoginRequest;
import com.garageapp.ServiceManagerService.payload.request.SignupRequest;
import com.garageapp.ServiceManagerService.payload.request.UpdateProfileRequest;
import com.garageapp.ServiceManagerService.payload.response.JwtResponse;
import com.garageapp.ServiceManagerService.payload.response.MessageResponse;
import com.garageapp.ServiceManagerService.repository.ServiceManagerRepository;
import com.garageapp.ServiceManagerService.security.jwt.JwtUtils;
import com.garageapp.ServiceManagerService.security.services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    ServiceManagerRepository servicemanagerrepository;
    @Autowired
    PasswordEncoder encoder;
    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        return ResponseEntity.ok(new JwtResponse(
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                userDetails.getName(),
                userDetails.getPhone(),
                userDetails.getUserType(),
                jwt));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@RequestBody SignupRequest signUpRequest) {
        if (servicemanagerrepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }
        if (servicemanagerrepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }
        // Create new user's account
        Service_Manager service_manager = new Service_Manager(
                signUpRequest.getUsername(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()),
                signUpRequest.getName(),
                signUpRequest.getPhone(),
                signUpRequest.getUserType());

        servicemanagerrepository.save(service_manager);
        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    @PutMapping("/updateprofile")
    ResponseEntity<?> UpdateUserProfile(@RequestBody UpdateProfileRequest updateProfileRequest){

        Service_Manager existingServiceManager = servicemanagerrepository.findByEmail(updateProfileRequest.getEmail());


        existingServiceManager.setUsername(updateProfileRequest.getUsername());
        existingServiceManager.setEmail(updateProfileRequest.getEmail());
        existingServiceManager.setPassword(encoder.encode(updateProfileRequest.getPassword()));
        existingServiceManager.setName(updateProfileRequest.getName());
        existingServiceManager.setPhone(updateProfileRequest.getPhone());

        Service_Manager result = servicemanagerrepository.save(existingServiceManager);

        return ResponseEntity.ok(new MessageResponse("User Updates successfully!"));
    }
}