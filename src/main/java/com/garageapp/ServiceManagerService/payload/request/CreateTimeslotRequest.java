package com.garageapp.ServiceManagerService.payload.request;

public class CreateTimeslotRequest {

    private int manager_Id;
    private String manager_Username;
    private String date;
    private String start_time;
    private String end_time;
    private int booking_limit;
    private int num_of_booking;


    public CreateTimeslotRequest(int manager_Id, String manager_Username, String date, String start_time, String end_time, int booking_limit, int num_of_booking) {
        this.manager_Id = manager_Id;
        this.manager_Username = manager_Username;
        this.date = date;
        this.start_time = start_time;
        this.end_time = end_time;
        this.booking_limit = booking_limit;
        this.num_of_booking = num_of_booking;
    }

    public int getManager_Id() {
        return manager_Id;
    }

    public void setManager_Id(int manager_Id) {
        this.manager_Id = manager_Id;
    }

    public String getManager_Username() {
        return manager_Username;
    }

    public void setManager_Username(String manager_Username) {
        this.manager_Username = manager_Username;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public int getBooking_limit() {
        return booking_limit;
    }

    public void setBooking_limit(int booking_limit) {
        this.booking_limit = booking_limit;
    }

    public int getNum_of_booking() {
        return num_of_booking;
    }

    public void setNum_of_booking(int num_of_booking) {
        this.num_of_booking = num_of_booking;
    }
}
